"""nwclabour URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import labour_site.views
import blog.views
import about.urls
import meetings.urls
import forms.urls

admin.site.site_header = "Labour Site"

urlpatterns = [
    path('contact/', labour_site.views.contact, name="contact"),
    path('news/<post_slug>', blog.views.post, name='post'),
    path('news/', blog.views.news, name='news'),

    path('about/', include(about.urls)),
    path('meetings/', include(meetings.urls)),

    path('forms/', include(forms.urls)),

    path('admin/', admin.site.urls),
    path('', labour_site.views.home, name='home')
]

if settings.DEBUG:
    admin.site.site_header = 'Labour Site (Dev)'
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
