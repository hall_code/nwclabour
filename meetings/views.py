from django.shortcuts import render, get_object_or_404
import datetime
from django.core.paginator import Paginator
from .models import Meeting
from about.models import Unit, Motion

def all_meetings(request, page_no=1):
    meetings = Meeting.objects.all()

    p = Paginator(meetings, 20)

    return render(request, 'pages/meetings.html', {
        "page": {
            "title": "Meetings",
            "description": "A list of meetings taking place in our CLP.",
        },
        "meetings": p.page(page_no),
        "p": p
    })

def meeting(request, unit, year, month, day):
    date = datetime.date(year=year, month=month, day=day)
    unit_obj = get_object_or_404(Unit, slug=unit)
    meeting = get_object_or_404(Meeting, unit=unit_obj, date=date)

    return render(request, 'pages/meeting.html', {
        "page": {
            "title": meeting,
            "description": "Information about %s" % (meeting,),
            "type": "agenda",
        },
        "meeting": meeting
    })



def meeting_minutes(request, unit, year, month, day):
    date = datetime.date(year=year, month=month, day=day)
    unit_obj = get_object_or_404(Unit, slug=unit)
    meeting = get_object_or_404(Meeting, unit=unit_obj, date=date)

    return render(request, 'pages/meeting.html', {
        "page": {
            "title": meeting,
            "description": "Information about %s" % (meeting,),
            "type": "minutes",
        },
        "meeting": meeting
    })


def motion(request, unit, year, month, day, motion):
    date = datetime.date(year=year, month=month, day=day)
    unit_obj = get_object_or_404(Unit, slug=unit)
    meeting = get_object_or_404(Meeting, unit=unit_obj, date=date)

    motion = get_object_or_404(Motion, pk=motion)

    return render(request, 'pages/motion.html', {
        "motion": motion,
        "meeting": meeting,
        "page": {
            "title": 'Motion - %s' % motion.title,
            "previous": "meeting",
            "description": "Text of a motion put to a meeting: %s" % (meeting,),
        }
    })