from django.urls import path
from .views import meeting, motion, all_meetings, meeting_minutes

urlpatterns = [
    path('', all_meetings, name="meetings"),
    path('page:<int:page>', all_meetings, name="meetings_page"),
    path('<slug:unit>/<int:year>-<int:month>-<int:day>', meeting, name="meeting"),

    path('<slug:unit>/<int:year>-<int:month>-<int:day>/agenda', meeting, name="agenda"),
    path('<slug:unit>/<int:year>-<int:month>-<int:day>/minutes', meeting_minutes, name="minutes"),

    path('<slug:unit>/<int:year>-<int:month>-<int:day>/motion/<int:motion>', motion, name="meeting_motion"),
]