from django.db import models
import datetime
import markdown2
from about.models import Unit, Motion


class Meeting(models.Model):
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, related_name="meetings")
    date = models.DateField()
    time = models.TimeField()
    address = models.CharField(max_length=1200)
    notes = models.TextField(null=True, blank=True, help_text="This will be small print on the agenda. Use it for parking instructions etc.")
    attendees = models.TextField(null=True, blank=True,
                                   help_text="For the minutes, write a comma separated list of names of people who attended.")
    apologies = models.TextField(null=True, blank=True,
                                 help_text="For the minutes, write a comma separated list of names of people who attended.")

    def __str__(self):
        return '%s - %s' % (self.unit.name, self.get_date())

    def in_future(self):
        today = datetime.date.today()
        return self.date >= today

    def get_agenda_items(self):
        return AgendaItem.objects.filter(meeting=self)

    def count_agenda_items(self):
        return len(self.get_agenda_items())

    def get_motion_item(self):
        return self.count_agenda_items() + 1

    def get_motions(self):
        motions = Motion.objects.filter(meeting=self).exclude(status__in=[
            ' ', 'W', 'R'
        ])

        return motions

    def get_attachments(self):
        return Attachment.objects.filter(meeting=self)

    def get_date(self):
        return self.date.strftime("%b %Y")

    class Meta:
        unique_together = ['unit', 'date']
        ordering = [
            '-date',
            '-time'
        ]

class AgendaItem(models.Model):
    meeting = models.ForeignKey(Meeting, on_delete=models.CASCADE, related_name="agenda_items")
    title = models.CharField(max_length=300)
    description = models.TextField(max_length=1500, null=True, blank=True,
                                   help_text="Add an explanation for the agenda.")

    minutes_notes = models.TextField(null=True, blank=True,
                                     help_text="Add the minutes notes for this item")

    order = models.IntegerField()

    def __str__(self):
        return '%s. %s' % (self.get_item_number(), self.title)

    def get_item_number(self):
        previous_items = AgendaItem.objects.filter(meeting=self.meeting, order__lt=self.order)
        return previous_items.count() + 1

    def get_minutes_notes_html(self):
        return markdown2.markdown(self.minutes_notes)

    def get_description_html(self):
        return markdown2.markdown(self.description)

    class Meta:
        ordering = [
            '-meeting__date',
            'order'
        ]

class Attachment(models.Model):
    title = models.CharField(max_length=200)
    file = models.FileField(upload_to='uploads/%Y/%m/')
    meeting = models.ForeignKey(Meeting, on_delete=models.CASCADE)

    def __str__(self):
        return self.title