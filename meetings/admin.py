from django.contrib import admin
from .models import AgendaItem, Meeting, Attachment

class AttachmentInline(admin.TabularInline):
    model = Attachment
    extra = 0

class AgendaItemInline(admin.StackedInline):
    model = AgendaItem
    extra = 0

@admin.register(Meeting)
class MeetingAdmin(admin.ModelAdmin):
    list_display = ('unit', 'get_date')
    inlines = [
        AttachmentInline,
        AgendaItemInline
    ]
