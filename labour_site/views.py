from django.shortcuts import render, redirect
from labour_site.models import MenuItem
from blog.models import Post

def home(request):
    infoMenu = MenuItem.objects.filter(menu="info")

    header = {
        "expanded": True,
        "title": "North West Cambridgeshire",
        "message": "We campaign in the North-West Cambs constituency to make lives better for every day people, and to bring about  the serious change in this country we all need to live better lives.",
        "buttons": [
            {"href": "https://join.labour.org.uk", "text":"Join Now"},
            {"href": "/about/policy", "text": "Read More"}
        ],
        "links": [
            {"href": "/news", "text": "Latest News"},
            {"href": "/about", "text": "Who We Are"},
            {"href": "/about/policy", "text": "Our Policies"},
            {"href": "/forms/Contact", "text": "Contact"},
            {"href": "/forms/Donate", "text": "Donate"}
        ]
    }

    page = {
        "title": "Home",
        "description": header["message"],
    }

    latestPosts = Post.objects.filter(status=" ").order_by('-created_at')[:2]

    return render(request, 'pages/landing.html', {
        "header": header,
        "page": page,
        "infoMenu": infoMenu,
        "posts": latestPosts
    })

def contact(request):
    return redirect('form.start', form='Contact')
