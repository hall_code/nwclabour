from django.db import models
from about.models import Unit, Person

class MenuItem(models.Model):
    MENUES = (
        ("main", "Header Menu"),
        ("callout", "Home Page Banner Menu"),
        ("info", "Information Menu")
    )
    title = models.CharField(max_length=350)
    url = models.CharField(max_length=1024)
    description = models.TextField(max_length=1024, null=True, blank=True)
    icon_class = models.CharField(max_length=100, null=True, blank=True)
    menu = models.CharField(choices=MENUES, max_length=20)
    more_text = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title

class Message(models.Model):
    token = models.CharField(max_length=100, unique=True)
    unit = models.ForeignKey(Unit, on_delete=models.SET_NULL, null=True)
    recipient = models.ForeignKey(Person, on_delete=models.SET_NULL, null=True)
    sender_name = models.CharField(max_length=250, null=True)
    sender_email = models.EmailField(null=True)
    subject = models.CharField(max_length=1024, null=True)
    message = models.TextField(null=True)
    sent = models.BooleanField(null=True)
