from django.apps import AppConfig


class LabourSiteConfig(AppConfig):
    name = 'labour_site'
