def add_header(request):
    return {
        "header": {
            "expanded": False,
            "title": "North West Cambridgeshire",
            "message": "We campaign in the North-West Cambs constituency to make lives better for every day people, and to bring about  the serious change in this country we all need to live better lives.",
            "links": [
                {"href": "/news", "text": "Latest News"},
                {"href": "/about", "text": "Who We Are"},
                {"href": "/about/policy", "text": "Our Policies"},
                {"href": "/forms/Contact", "text": "Contact"},
                {"href": "/forms/Donate", "text": "Donate"}
            ]
        }
    }


# header = {
#     "expanded": False,
#     "title": "North West Cambridgeshire",
#     "message": "We campaign in the North-West Cambs constituency to make lives better for every day people, and to bring about  the serious change in this country we all need to live better lives.",
#     "buttons": [
#         {"href": "https://join.labour.org.uk", "text": "Join Now"},
#         {"href": "/about", "text": "Read More"}
#     ],
#     "links": [
#         {"href": "/news", "text": "Latest News"},
#         {"href": "/about", "text": "Who We Are"},
#         {"href": "/contact", "text": "Contact"}
#     ]
# }