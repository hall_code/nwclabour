from about.models import Unit
from django.shortcuts import render

class SiteDownMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path[1:6] != 'admin':
            try:
                obj = Unit.objects.get(default_unit=True)
            except Unit.DoesNotExist:
                header = {
                    "expanded": False,
                    "title": "Your Local CLP",
                    "links": []
                }

                page = {
                    "title": "Oops"
                }

                return render(request, 'error/oops.html', {
                    "header": header,
                    "page": page,
                })

        response = self.get_response(request)
        return response