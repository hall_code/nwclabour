{% extends 'emails/txt_template.txt' %}

{% block content %}
Dear {{ motion.submitted_by }},

Thank you for submitting a motion to {{ motion.submitted_to }}. It was received on {{ motion.submitted_on }}.

It will now be considered by the Chair of {{ motion.submitted_to }}, who will either schedule it to be debated
at the next available meeting, or may reject it (if it fails to meet the criteria for a motion.
Either way, we'll email you with the details when this happens.

For your reference, a copy of the text of the motion is included below.

Many thanks,
{{ motion.submitted_to }}

{{ motion.title }}

{{ motion.html }}
{% endblock %}