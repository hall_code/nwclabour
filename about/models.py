from django.db import models
from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist
import markdown2
import datetime
import calendar

class Policy(models.Model):
    headline = models.CharField(max_length=250)
    description = models.TextField()
    icon_class = models.CharField(max_length=25)
    color = models.CharField(max_length=25, null=True, blank=True, help_text="You can use any valid html color value.")

    def __str__(self):
        return self.headline

    def html(self):
        return markdown2.markdown(self.description)

    class Meta:
        verbose_name_plural = "policies"

class Person(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='uploads/avatars/', null=True, blank=True)
    email = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "people"

class Unit(models.Model):
    TYPES = (
        ("CLP", "Constituency Labour Party"),
        ("BLP", "Branch Labour Party"),
        ("LCF", "Local Campaign Forum"),
        ("Group", "Labour Group"),
        ("Committee", "Committee"),
    )
    MEMBERSHIPS = (
        ("AM", "All members in the constituency"),
        ("BM", "All members in branch"),
        ("EM", "Elected members only")
    )
    DAYS = (
        (1, "Monday"),
        (2, "Tuesday"),
        (3, "Wednesday"),
        (4, "Thursday"),
        (5, "Friday"),
        (6, "Saturday"),
        (7, "Sunday")
    )
    name = models.CharField(max_length=250)
    slug = models.SlugField(unique=True, null=True)
    type = models.CharField(max_length=10, choices=TYPES)
    description = models.TextField()
    membership = models.CharField(max_length=3, choices=MEMBERSHIPS)
    members = models.ManyToManyField(Person, related_name='committees', through='Role')
    week_of_month = models.IntegerField(null=True)
    day_of_week = models.IntegerField(null=True, choices=DAYS)
    default_unit = models.BooleanField(default=False)

    class Meta:
        ordering = ['-default_unit', 'type']

    def __str__(self):
        return '%s %s' % (self.name, self.type)

    def nextMeeting(self):
        today = datetime.date.today()
        this_month_date = self.dateInMonth(year=today.year, month=today.month)

        if this_month_date < today:
            next_month = today + datetime.timedelta(days=32)
            return self.dateInMonth(year=next_month.year, month=next_month.month)

        return this_month_date

    def is_next_meeting_in_calendar(self):
        try:
            Meeting = apps.get_model('meetings', 'Meeting')
            Meeting.objects.get(unit=self, date=self.nextMeeting())
            return True
        except ObjectDoesNotExist:
            return False
    
    def dateInMonth(self, month, year):
        month_range = calendar.monthcalendar(year, month)
        week = self.week_of_month - 1
        day_of_week = self.day_of_week - 1
        first_week = month_range[0]
        first_week_day = first_week[day_of_week]

        if first_week_day == 0:
            week += 1

        while week > len(month_range) - 1:
            week -= 1

        date = month_range[week][day_of_week]

        while date == 0:
            week -= 1
            date = month_range[week][day_of_week]

        full_date = datetime.date(year, month, date)

        return full_date

class Role(models.Model):
    TITLES = (
        (1, "Chair"),
        (2, "Vice Chair"),
        (3, "Secretary"),
        (4, "Treasurer"),
        (5, "Vice Chair Membership"),
        (6, "Woman's Officer"),
        (7, "Policy Officer"),
        (8, "BAME Officer"),
        (9, "Disability Officer"),
        (10, "LGBT+ Officer"),
        (11, "Youth Officer"),
        (12, "Trade Union Liaison Officer"),
        (13, "Political Education Officer"),
        (14, "Communications and Social Media Officer"),
        (15, "Campaign Coordinator"),
        (16, "Procedures Secretary"),
        (17, "Selection Coordinator"),
        (21, "Member of Parliament"),
        (22, "Police & Crime Commissioner"),
        (31, "Group Leader"),
        (32, "Cabinet Member"),
        (33, "County Councillor"),
        (34, "City Councillor"),
        (35, "District Councillor"),
        (36, "Parish Councillor"),
        (50, "Delegate"),
        (51, "Observer"),
        (60, "Member"),
    )
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True)
    term_ends_on = models.DateField()
    title = models.IntegerField(choices=TITLES)
    sub_title = models.CharField(max_length=200, null=True, blank=True, help_text="Authority/Ward name, or position title (i.e. for cabinet member,'Children's Services').")
    hide = models.BooleanField(default=False)

    class Meta:
        ordering = ['title', '-term_ends_on']

    def __str__(self):
        unit = self.unit
        if unit == None:
            unit = 'Constituency'

        return '%s, %s' % (self.title, unit)

class Motion(models.Model):
    STATUSES = (
        (" ", "Received"),
        ("R", "Rejected"),
        ("A", "Awaiting debate"),
        ("P", "Passed"),
        ("L", "Lost"),
        ("W", "Withdrawn"),
    )
    REJECTION_REASONS = (
        (
            "it is not clear what the motion is asking for.",
            "Unclear"
        ),
        (
            "the motion is against Labour Party rules or is otherwise out of order.",
            "Out of order"
        ),
        (
            "the motion is not directed towards a valid recipient.",
            "Invalid/No Recipient"
        ),
        (
            "a similar motion has recently been heard, is already scheduled, or this is otherwise a duplicate of an identical motion already dealt with.",
            "Duplicate"
        ),
        (
            "the motion is not directed towards a valid recipient.",
            "Invalid/No Recipient"
        ),
        (
            "this motion is vexatious; it has been submitted to frustrate the meeting or is not a serious motion.",
            "Vexatious"
        ),
    )
    title = models.CharField(max_length=1024)
    text = models.TextField()
    submitted_on = models.DateField()
    submitted_to = models.ForeignKey(Unit, on_delete=models.CASCADE)
    submitted_by = models.CharField(max_length=200)
    submitted_by_email = models.EmailField(blank=True, null=True)
    meeting = models.ForeignKey('meetings.Meeting', on_delete=models.SET_NULL, null=True, blank=True)
    debated_on = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=1, choices=STATUSES, default=" ")
    rejected_reason = models.CharField(max_length=1250, null=True, blank=True, choices=REJECTION_REASONS)

    def __str__(self):
        return self.title

    def html(self):
        return markdown2.markdown(self.text)

    def scheduled_for(self):
        if self.meeting:
            return self.meeting.date

        return None

    def status_icon(self):
        STATUS_ICONS = {
            " ": "check",
            "R": "times",
            "A": "hourglass",
            "P": "thumbs-up",
            "L": "thumbs-down",
            "W": "minus",
        }

        return 'fas fa-%s' % (STATUS_ICONS[self.status],)

    def status_text(self):
        STATUS_MESSAGES = {
            " ": "This motion was received on %s and is awaiting approval." % (format_date(self.submitted_on),),
            "R": "This motion was rejected by %s because %s" % (self.submitted_to, self.rejected_reason),
            "A": "This motion will be debated on %s by %s" % (format_date(self.scheduled_for()), self.submitted_to),
            "P": "This motion was debated by %s on %s and was agreed." % (self.submitted_to, format_date(self.debated_on)),
            "L": "This motion was debated by %s on %s but not agreed." % (self.submitted_to, format_date(self.debated_on)),
            "W": "This motion was withdrawn by its author.",
        }

        return STATUS_MESSAGES[self.status]

    def get_date(self):
        if self.status == ' ' or self.status == 'R' or self.status == 'W' or self.meeting is None:
            return format_date(self.submitted_on, False)

        if self.meeting:
            return format_date(self.scheduled_for(), False)

        return format_date(self.debated_on, False)

    class Meta:
        ordering = ['-meeting__date', 'submitted_on']

def format_date(date, long=True):
    if date is not None:
        if long:
            return date.strftime("%A, %-d %b %Y")

        return date.strftime("%B %Y")


    return date