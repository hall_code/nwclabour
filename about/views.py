from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from .models import Motion, Unit, Role, Policy
import datetime

def all_motions(request, page_no=1):
    motions = Motion.objects.exclude(status="W").exclude(status=" ")

    p = Paginator(motions, 20)

    return render(request, 'pages/motions.html', {
        "page": {
            "title": "Motions",
            "description": "A list of all motions passed at our meetings. Motions are the main way we make democratic decisions.",
        },
        "motions": p.page(page_no),
        "p": p
    })

def motion(request, motion_id):
    motion = get_object_or_404(Motion, pk=motion_id)

    return render(request, 'pages/motion.html', {
        "motion": motion,
        "page": {
            "title": 'Motion - %s' % motion.title,
            "previous": "motions",
            "description": "Motion: %s, Excerpt: %s" % (motion.title, motion.text[:150]+'...'),
        }
    })

def default_unit(request):
    today = datetime.date.today()

    clp = Unit.objects.get(default_unit=True)
    child_units = Unit.objects.exclude(default_unit=True, type="CLP")
    roles = Role.objects.filter(unit=clp, title__lt=20).exclude(term_ends_on__lt=today)
    elected = Role.objects.filter(title__gte=20, title__lt=40).exclude(term_ends_on__lt=today)
    members = Role.objects.filter(unit=clp, title__gte=50).exclude(term_ends_on__lt=today)

    return render(request, 'pages/unit.html', {
        "page": {
            "title": clp,
            "description": "Information about the Labour Party in the %s parliamentary constituency." % (clp.name,),
        },
        "unit": clp,
        "roles": roles,
        "elected": elected,
        "members": members,
        "branches": child_units
    })

def unit(request, unit_type, unit_slug):
    today = datetime.date.today()

    org = Unit.objects.get(type=unit_type, slug=unit_slug)
    roles = Role.objects.filter(unit=org, title__lt=20).exclude(term_ends_on__lt=today)
    elected = Role.objects.filter(unit=org, title__gte=20, title__lt=40).exclude(term_ends_on__lt=today)
    members = Role.objects.filter(unit=org, title__gte=50).exclude(term_ends_on__lt=today)

    return render(request, 'pages/unit.html', {
        "page": {
            "title": org,
            "description": "Each Constituency Labour Party is split into branches and committees, this page is about our %s." % (org,),
        },
        "unit": org,
        "roles": roles,
        "elected": elected,
        "members": members
    })

def policy(request):
    policies = Policy.objects.all()
    context = {
        "page": {
            "title": "Our Policies",
            "description": "What we stand for and our policies.",
        },
        "policies": policies
    }
    return render(request, 'pages/policy.html', context)