from django.urls import path
from .views import default_unit, all_motions, motion, unit, policy

urlpatterns = [
    path('', default_unit, name="clp"),
    path('policy/', policy, name="policy"),
    path('motions/', all_motions, name="motions"),
    path('motions/page:<page_no>', all_motions, name="motions_page"),
    path('motions/<motion_id>', motion, name="motion"),
    path('<unit_type>/<unit_slug>', unit, name="unit"),
]