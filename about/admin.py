from django.contrib import admin
from .models import Policy, Person, Unit, Role, Motion

@admin.register(Policy)
class PolicyAdmin(admin.ModelAdmin):
    list_display = ('headline',)

class PeopleRolesInline(admin.TabularInline):
    model = Role
    fk_name = "person"

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')
    inlines = [
        PeopleRolesInline
    ]

class UnitRolesInline(admin.TabularInline):
    model = Role
    fk_name = "unit"

@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')
    inlines = [
        UnitRolesInline
    ]
    prepopulated_fields = {"slug": ("name","type")}

@admin.register(Motion)
class MotionAdmin(admin.ModelAdmin):
    list_display = ('title', 'submitted_by', 'submitted_to', 'status', 'submitted_on')


