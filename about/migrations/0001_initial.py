# Generated by Django 2.2.5 on 2019-10-29 21:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Motion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=1024)),
                ('text', models.TextField()),
                ('submitted_on', models.DateField()),
                ('submitted_by', models.CharField(max_length=200)),
                ('submitted_by_email', models.EmailField(blank=True, max_length=254, null=True)),
                ('debated_on', models.DateField(blank=True, null=True)),
                ('status', models.CharField(choices=[(' ', 'Received'), ('R', 'Rejected'), ('A', 'Awaiting debate'), ('P', 'Passed'), ('L', 'Lost'), ('W', 'Withdrawn')], default=' ', max_length=1)),
                ('rejected_reason', models.CharField(blank=True, choices=[('it is not clear what the motion is asking for.', 'Unclear'), ('the motion is against Labour Party rules or is otherwise out of order.', 'Out of order'), ('the motion is not directed towards a valid recipient.', 'Invalid/No Recipient'), ('a similar motion has recently been heard, is already scheduled, or this is otherwise a duplicate of an identical motion already dealt with.', 'Duplicate'), ('the motion is not directed towards a valid recipient.', 'Invalid/No Recipient'), ('this motion is vexatious; it has been submitted to frustrate the meeting or is not a serious motion.', 'Vexatious')], max_length=1250, null=True)),
            ],
            options={
                'ordering': ['-meeting__date', 'submitted_on'],
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('photo', models.ImageField(blank=True, null=True, upload_to='uploads/avatars/')),
                ('email', models.CharField(blank=True, max_length=1024, null=True)),
            ],
            options={
                'verbose_name_plural': 'people',
            },
        ),
        migrations.CreateModel(
            name='Policy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline', models.CharField(max_length=250)),
                ('description', models.TextField()),
                ('icon_class', models.CharField(max_length=25)),
                ('color', models.CharField(blank=True, help_text='You can use any valid html color value.', max_length=25, null=True)),
            ],
            options={
                'verbose_name_plural': 'policies',
            },
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('term_ends_on', models.DateField()),
                ('title', models.IntegerField(choices=[(1, 'Chair'), (2, 'Vice Chair'), (3, 'Secretary'), (4, 'Treasurer'), (5, 'Vice Chair Membership'), (6, "Woman's Officer"), (7, 'Policy Officer'), (8, 'BAME Officer'), (9, 'Disability Officer'), (10, 'LGBT+ Officer'), (11, 'Youth Officer'), (12, 'Trade Union Liaison Officer'), (13, 'Political Education Officer'), (14, 'Communications and Social Media Officer'), (15, 'Campaign Coordinator'), (16, 'Procedures Secretary'), (17, 'Selection Coordinator'), (21, 'Member of Parliament'), (22, 'Police & Crime Commissioner'), (31, 'Group Leader'), (32, 'Cabinet Member'), (33, 'County Councillor'), (34, 'City Councillor'), (35, 'District Councillor'), (36, 'Parish Councillor'), (50, 'Delegate'), (51, 'Observer'), (60, 'Member')])),
                ('sub_title', models.CharField(blank=True, help_text="Authority/Ward name, or position title (i.e. for cabinet member,'Children's Services').", max_length=200, null=True)),
                ('hide', models.BooleanField(default=False)),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='about.Person')),
            ],
            options={
                'ordering': ['title', '-term_ends_on'],
            },
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('slug', models.SlugField(null=True, unique=True)),
                ('type', models.CharField(choices=[('CLP', 'Constituency Labour Party'), ('BLP', 'Branch Labour Party'), ('LCF', 'Local Campaign Forum'), ('Group', 'Labour Group'), ('Committee', 'Committee')], max_length=10)),
                ('description', models.TextField()),
                ('membership', models.CharField(choices=[('AM', 'All members in the constituency'), ('BM', 'All members in branch'), ('EM', 'Elected members only')], max_length=3)),
                ('week_of_month', models.IntegerField(null=True)),
                ('day_of_week', models.IntegerField(choices=[(1, 'Monday'), (2, 'Tuesday'), (3, 'Wednesday'), (4, 'Thursday'), (5, 'Friday'), (6, 'Saturday'), (7, 'Sunday')], null=True)),
                ('default_unit', models.BooleanField(default=False)),
                ('members', models.ManyToManyField(related_name='committees', through='about.Role', to='about.Person')),
            ],
            options={
                'ordering': ['-default_unit', 'type'],
            },
        ),
        migrations.AddField(
            model_name='role',
            name='unit',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='about.Unit'),
        ),
    ]
