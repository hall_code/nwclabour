from django.contrib import admin
from blog.models import Post
from blog.models import Category

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('headline', 'status', 'created_at', 'category')
    prepopulated_fields = {"slug": ("headline",)}

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}