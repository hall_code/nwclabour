from django.db import models
import markdown2

class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"

class Post(models.Model):
    STATUSES = (
        (" ", "Published"),
        ("D", "Draft"),
        ("A", "Archived")
    )

    headline = models.CharField(max_length=500)
    slug = models.SlugField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUSES)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name="posts")
    text = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='uploads/%Y/%m/', null=True, blank=True)

    def __str__(self):
        return self.headline

    def html(self):
        return markdown2.markdown(self.text)

    def description(self, length=250):
        if len(self.text) < length:
            return self.text

        html = markdown2.markdown(self.text[:length-3] + '...')

        return html