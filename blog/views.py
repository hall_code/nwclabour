from django.shortcuts import render, get_object_or_404
from .models import Post, Category
from labour_site.models import MenuItem

def post(request, post_slug):
    infoMenu = MenuItem.objects.filter(menu="info")
    article = get_object_or_404(Post, slug=post_slug)

    # latestPosts = Post.objects.filter(status=" ").order_by('-created_at')[:2]

    return render(request, 'pages/post.html', {
        "article": article,
        "infoMenu": infoMenu,
        "page": {
            "title": article.headline,
            "description": article.text[:200]+'...',
        }
    })

def news(request):
    infoMenu = MenuItem.objects.filter(menu="info")
    posts = Post.objects.filter(status=" ").order_by('-created_at')[:10]

    return render(request, 'pages/news.html', {
        "posts": posts,
        "page": {
            "title": "News",
            "description": "The latest news and information about this local Labour Party.",
        }
    })