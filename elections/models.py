from django.db import models
from about.models import Person

class Area(models.Model):
    TYPES = (
        ("ward", "Ward"),
        ("constituency", "Constituency"),
        ("division", "Division")
    )
    name = models.CharField(max_length=200, unique=True)
    branch = models.ForeignKey(Unit, related_name='wards',
                               limit_choices_to={
                                   'type': 'BLP'
                               }, on_delete=models.PROTECT)
    type = models.CharField(max_length=100, choices=TYPES)

    def __str__(self):
        return self.name

class Election(models.Model):
    TYPES = (
        ("general", "UK General"),
        ("local-ua", "Local Government (Unitary Authority)"),
        ("local-district", "Local Government (District)"),
        ("local-county", "County Council"),
        ("pcc", "Police & Crime Commissioner")
    )
    type = models.CharField(max_length=30, choices=TYPES)
    date = models.DateField()
    candidates = models.ManyToManyField(Person, related_name='elections', through='candidacy')

class Candidacy(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    election = models.ForeignKey(Election, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    hide = models.BooleanField(default=False)

    def __str__(self):
        return '%s, %s' % (self.title, self.unit)

    class Meta:
        verbose_name_plural = "candidacies"