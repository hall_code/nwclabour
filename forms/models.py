from django.db import models

class Payment(models.Model):
    STATUSES = (
        (' ', 'Pending'),
        ('F', 'Failed'),
        ('P', 'Payed')
    )

    timestamp = models.DateTimeField(auto_now=True)
    token = models.CharField(max_length=100, unique=True)
    form = models.CharField(max_length=30)
    name = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=1024, null=True)
    address = models.CharField(max_length=500, null=True)
    amount = models.DecimalField(max_digits=5, decimal_places=2)
    status = models.CharField(max_length=1, default=' ')
    decline_code = models.CharField(max_length=30, null=True)
