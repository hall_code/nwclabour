import stripe, secrets, json
from django.conf import settings
from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from .models import Payment

from .schemas.contact import ContactForm
from .schemas.donate import DonationForm
from .schemas.survey import VolunteerSurvey

def get_form_from_slug(slug):
    forms = {
        "Contact": ContactForm(),
        "Donate": DonationForm(),
        "volunteersurvey": VolunteerSurvey()
    }

    return forms[slug]

def forms(request):
    raise Http404("This page does not exist!")

def form(request, form):
    try:
        form = get_form_from_slug(form)
    except:
        raise Http404("Form does not exist")

    if form.start_template is not None:
        return form.get_start_page(request)

    return redirect('form.do', form=form.slug)

def questions(request, form):
    try:
        form = get_form_from_slug(form)
    except:
        raise Http404("Form does not exist")

    return render(request, 'forms/page.html', {
        "form": form,
        "page": {
            "title": form.title
        }
    })

def pay(request, form):
    try:
        form = get_form_from_slug(form)
    except:
        raise Http404("Form does not exist")

    if request.method == 'GET':
        return show_payment_form(request, form)

    if request.method == 'POST':
        return charge(request, form)

    return JsonResponse({
        "error": {
            "message": "Method not allowed."
        }
    }, status=405)

def show_payment_form(request, form):
    token_key = "%s_payment_token" % (form.slug,)

    if token_key not in request.session:
        raise BaseException
    else:
        payment = Payment.objects.get(token=request.session[token_key])

    if payment.status == 'P':
        return form.get_end_page(request)

    return render(request, 'forms/payment.html', {
        "form": form,
        "payment": payment,
        "page": {
            "title": form.title + ' Payment'
        },
        "key": settings.STRIPE_KEY
    })

def charge(request, form):
    stripe.api_key = settings.STRIPE_SECRET

    token_key = "%s_payment_token" % (form.slug,)

    payment = Payment.objects.get(token=request.session[token_key])

    if payment.status == 'P':
        return JsonResponse({
            "error": {
                "message": "The payment has already been made."
            }
        }, status=400)

    body = json.loads(request.body)

    try:
        if 'payment_method_id' in body:
                method_id = body['payment_method_id']

                # Create the PaymentIntent
                intent = stripe.PaymentIntent.create(
                    payment_method=method_id,
                    amount=int(payment.amount * 100),
                    currency='gbp',
                    confirmation_method='manual',
                    confirm=True,
                )

        elif 'payment_intent_id' in body:
            intent = stripe.PaymentIntent.confirm(body['payment_intent_id'])

        else:
            return JsonResponse({
                "error": {
                    "message": "The was an error creating the payment request."
                }
            }, status=400)

    except stripe.error.CardError as e:
        return JsonResponse({
            "error": {
                "message": e.user_message
            }
        }, status=200)

    try:
        payment.name = body['person_details']['name']
        payment.address = '%s, %s, %s, %s' % (
            body['person_details']['address']['line1'],
            body['person_details']['address']['line2'],
            body['person_details']['address']['city'],
            body['person_details']['address']['postal_code']
        )
        payment.email = body["person_details"]["email"]

        payment.save()
    except KeyError:
        pass

    return generate_payment_response(intent, payment)


def done(request, form):
    try:
        form = get_form_from_slug(form)
    except:
        raise Http404("Form does not exist")

    form.submit(request)

    if form.payment:
        return redirect('form.pay', form=form.slug)

    return form.get_end_page(request)


# Handle the response for payments
def generate_payment_response(intent, payment):
  # Note that if your API version is before 2019-02-11, 'requires_action'
  # appears as 'requires_source_action'.
  if intent.status == 'requires_action' and intent.next_action.type == 'use_stripe_sdk':
    # Tell the client to handle the action
    return JsonResponse({
      'requires_action': True,
      'payment_intent_client_secret': intent.client_secret,
    }, status=200)

  elif intent.status == 'succeeded':
    # The payment didn’t need any additional actions and completed!
    # Handle post-payment fulfillment
    payment.status = 'P'
    payment.save()
    return JsonResponse({'success': True}, status=200)

  else:
    # Invalid status
    payment.status = 'F'
    payment.save()
    return JsonResponse({'error': {
        "message:": 'Invalid PaymentIntent status'
    }}, status=500)