from django.urls import path
from .views import forms, form, done, pay, questions

urlpatterns = [
    path('', forms, name="forms"),
    path('<form>/', form, name="form.start"),
    path('<form>/Do', questions, name="form.do"),
    path('<form>/Submit', done, name="form.submit"),
    path('<form>/Pay', pay, name="form.pay"),
    path('<form>/Done', done, name="form.done"),
]