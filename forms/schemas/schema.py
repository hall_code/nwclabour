import datetime, json, csv, os.path
from django.shortcuts import render
from django.conf import settings

class Schema:
    title = 'form'
    slug = 'form'
    start_template = 'default.html'
    end_template = 'default.html'
    opens_at = datetime.date.today()
    deadline = datetime.date.today()
    questions = []
    POST = {}
    payment = False

    def get_start_page(self, request):
        today = datetime.date.today()
        not_open = today < self.opens_at
        deadline_passed = today > self.deadline

        if deadline_passed or not_open:
            return render(request, 'forms/not_open.html', {
                "form": self,
                "deadline_passed": deadline_passed,
                "not_open": not_open,
                "today": today
            })

        return render(request, 'forms/start/' + self.start_template, {
            "form": self,
            "page": {
                "title": self.title
            }
        })

    def get_end_page(self, request):
        return render(request, 'forms/end/'+self.end_template, {
            "form": self,
            "page": {
                "title": self.title + ' Completed'
            }
        })

    def get_questions(self):
        return json.dumps(self.questions)

    def is_closed(self):
        not_open = datetime.date.today() < self.opens_at
        deadline_passed = datetime.date.today() > self.deadline

        return not_open or deadline_passed

    def get_payment_amount(self):
        return 0

    def submit(self, request):
        self.POST = request.POST

        if self.POST['_email'] != '':
            raise AttributeError

        try:
            self.on_submit(request)
        except AttributeError:
            pass

        self.log()

        return self.get_end_page(request)

    def log(self):
        path = os.path.join(settings.BASE_DIR, 'forms', 'logs', self.slug+'.csv')
        log = open(path, "a+")

        for i, field in enumerate(self.POST, start=1):
            log.write('"%s", ' % (self.POST[field],))

        log.write("\r\n")
        log.close()
