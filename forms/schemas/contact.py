import datetime
from .schema import Schema
from about.models import Unit, Role
from django.core.mail import EmailMessage
from django.utils.html import strip_tags

def get_units():
    units = Unit.objects.all()
    unit_options = []

    for u in units:
        unit_options.append({
            "value": u.pk,
            "label": str(u)
        })

    return unit_options

class ContactForm(Schema):
    db_units = Unit.objects.all()

    title = 'Contact'
    slug = 'Contact'
    start_template = None
    end_template = 'default.html'
    questions = [
        {
            "component": "ChoiceField",
            "unique": "unit",
            "max": 1,
            "options": get_units(),
            "required": True,
            "label": "Who do you want to contact?",
            "description": "Select the part of the local party you wish to contact.",
            "value": []
        },
        {
            "component": "ShortText",
            "unique": "name",
            "required": True,
            "type": "text",
            "label": "What is your name?",
            "description": "This is so we know who you are.",
            "value": None
        },
        {
            "component": "ShortText",
            "unique": "email",
            "required": True,
            "type": "email",
            "label": "What is your email address?",
            "description": "So we can get back to you.",
            "value": None
        },
        {
            "component": "ShortText",
            "unique": "subject",
            "required": True,
            "label": "What is the subject of your message?",
            "description": "What is your message about?",
            "value": None
        },
        {
            "component": "LongText",
            "unique": "message",
            "required": True,
            "label": "Your message",
            "description": "What do you want to say?",
            "value": None
        },
    ]

    def on_submit(self, request):
        unit = Unit.objects.get(pk=self.POST["unit[0]"])

        today = datetime.date.today()
        recipient = Role.objects.filter(unit=unit).exclude(term_ends_on__lt=today).exclude(person__email=None)[:1]
        recipient = recipient[0]

        email = EmailMessage(
            subject=self.POST["subject"],
            body='New message for %s:\n--------------------\n\n%s\n\n%s\n%s' % (
                str(unit),
                strip_tags(self.POST["message"]),
                self.POST["name"],
                self.POST["email"]
            ),
            from_email='"%s" <no_reply@northwestcambslabour.org.uk>' % (self.POST["name"],),
            reply_to=[
                '"%s" <%s>' % (self.POST["name"], self.POST["email"])
            ],
            to=[
                '"%s" <%s>' % (recipient.person.name, recipient.person.email)
            ],
        )

        email.send()