import os.path
from .schema import Schema
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

class VolunteerSurvey(Schema):
    title = 'Volunteer'
    slug = 'volunteersurvey'
    start_template = 'volunteer-survey.html'
    end_template = 'survey.html'
    questions = [
        {
            "component": "ChoiceOtherField",
            "unique": "previous_activity",
            "label": "Have you campaigned before?",
            "description": "Have you done any of the following activities for the Labour Party before?",
            "options": [
                {
                    "value": "Leafleting",
                    "label": "Leafleting"
                },
                {
                    "value": "Canvassing",
                    "label": "Door-knocking"
                },
                {
                    "value": "Canvassing leader",
                    "label": "Leading Voter ID sessions"
                },
                {
                    "value": "Phone banking",
                    "label": "Making phone calls"
                },
                {
                    "value": "Writing",
                    "label": "Writing blog posts / press releases"
                },
                {
                    "value": "Attending events",
                    "label": "Attending street events"
                },
                {
                    "value": "Organising events",
                    "label": "Organising street events"
                },
                {
                    "value": "Donating",
                    "label": "Donating"
                }
            ],
            "value": []
        },
        {
            "component": "ChoiceOtherField",
            "unique": "future_activity",
            "label": "Which of these would you do?",
            "description": "Which of the following would you be interested in helping with?",
            "options": [
                {
                    "value": "Leafleting",
                    "label": "Leafleting"
                },
                {
                    "value": "Canvassing",
                    "label": "Door-knocking"
                },
                {
                    "value": "Canvassing leader",
                    "label": "Leading Voter ID sessions"
                },
                {
                    "value": "Phone banking",
                    "label": "Making phone calls"
                },
                {
                    "value": "Writing",
                    "label": "Writing blog posts / press releases"
                },
                {
                    "value": "Attending events",
                    "label": "Attending street events"
                },
                {
                    "value": "Organising events",
                    "label": "Organising street events"
                },
            ],
            "value": []
        },
        {
            "component": "ChoiceField",
            "unique": "can_drive",
            "label": "Can you drive?",
            "description": "Do you drive and would you be available and willing to give lifts to activists"
                           "and/or voters on polling day?",
            "max": 1,
            "options": [
                {
                    "value": "Y",
                    "label": "Yes"
                },
                {
                    "value": "N",
                    "label": "No"
                }
            ],
            "value": []
        },
        {
            "component": "ChoiceOtherField",
            "unique": "skills",
            "label": "What else can you do?",
            "description": "Do you have skills? Let us know what you're good at.",
            "options": [
                {
                    "value": "Photography",
                    "label": "Photography"
                },
                {
                    "value": "Public speaking",
                    "label": "Public Speaking"
                },
                {
                    "value": "Video editing",
                    "label": "Video Editing"
                },
                {
                    "value": "Social media",
                    "label": "Social media influencing"
                },
                {
                    "value": "Writing",
                    "label": "Writing"
                },
                {
                    "value": "Coding",
                    "label": "Coding"
                },
                {
                    "value": "Graphic Design",
                    "label": "Graphic Design"
                },
                {
                    "value": "Numbers / Spreadsheets",
                    "label": "Numbers / Spreadsheets"
                },
                {
                    "value": "Sales / Marketing",
                    "label": "Sales / Marketing"
                }
            ],
            "value": []
        },
        {
            "component": "ChoiceField",
            "unique": "availability_times",
            "label": "Which times of day are you available?",
            "description": "Roughly speaking, which times of the day are you usually most available.",
            "options": [
                {
                    "value": "Morning",
                    "label": "Morning"
                },
                {
                    "value": "Afternoon",
                    "label": "Afternoon"
                },
                {
                    "value": "Evening",
                    "label": "Evening"
                },
            ],
            "value": []
        },
        {
            "component": "ChoiceField",
            "unique": "availability_days",
            "label": "Which days are you available?",
            "description": "Roughly speaking, which days are you usually most available.",
            "options": [
                {
                    "value": "Monday",
                    "label": "Monday"
                },
                {
                    "value": "Tuesday",
                    "label": "Tuesday"
                },
                {
                    "value": "Wednesday",
                    "label": "Wednesday"
                },
                {
                    "value": "Thursday",
                    "label": "Thursday"
                },
                {
                    "value": "Friday",
                    "label": "Friday"
                },
                {
                    "value": "Saturday",
                    "label": "Saturday"
                },
                {
                    "value": "Sunday",
                    "label": "Sunday"
                },
            ],
            "value": []
        },
        {
            "component": "ChoiceField",
            "unique": "is_member",
            "label": "Are you a member of the Labour Party?",
            "max": 1,
            "options": [
                {
                    "value": "y",
                    "label": "Yes"
                },
                {
                    "value": "n",
                    "label": "No"
                }
            ],
            "value": []
        },
        {
            "component": "ShortText",
            "unique": "post_code",
            "required": False,
            "type": "text",
            "style": "max-width: 10em",
            "label": "Post code",
            "description": "What's your post code?",
            "value": None,
            "order": 99
        },
        {
            "component": "ShortText",
            "unique": "email",
            "required": False,
            "type": "email",
            "label": "What is your email address?",
            "description": "We'll use this to tell you about events and campaign updates.",
            "value": None,
            "order": 100
        },
    ]

    def on_submit(self, request):
        view = render_to_string('emails/survey-response.html', {
            "post": self.POST,
            "title": "New survey response",
            "skills": get_list('skills', self.POST),
            "future_activity": get_list('future_activity', self.POST),
            "previous_activity": get_list('previous_activity', self.POST),
            "can_drive": get_list('can_drive', self.POST)[0],
            "is_member": get_list('is_member', self.POST)[0],
            "availability_days": get_list('availability_days', self.POST),
            "availability_times": get_list('availability_times', self.POST),
        })

        email = EmailMessage(
            subject='New volunteer submission',
            body=view,
            from_email='NWC Labour <no_reply@northwestcambslabour.org.uk>',
            reply_to=[
                self.POST["email"]
            ],
            to=[
                '"%s" <%s>' % ('Alex Hall', 'alexhall93@me.com'),
                '"David Fry" <david_fry_qcl@msn.com>'
            ],
        )

        email.content_subtype = "html"
        email.send(fail_silently=True)

    def log(self):
        path = os.path.join(settings.BASE_DIR, 'forms', 'logs', self.slug + '.csv')
        log = open(path, "a+")

        for i, field in enumerate(self.POST, start=1):
            response = self.POST[field]
            log.write('"%s", ' % (response,))

        log.write("\r\n")
        log.close()

def get_list(key, list):
    array = []
    keylen = len(key)
    for k in list:
        if key == k[:keylen]:
            array.append(list[k])

    return array