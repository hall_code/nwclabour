import datetime, secrets
from forms.models import Payment
from django.shortcuts import render
from .schema import Schema
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


def get_amounts():
    amounts = [3, 5, 10, 20, 50, 100, 250]
    amounts_with_card_charge = []

    for a in amounts:
        amounts_with_card_charge.append({
            "value": a,
            "label": '£%.0f' % (a,)
        })

    return amounts_with_card_charge


class DonationForm(Schema):
    title = 'Make a Donation'
    slug = 'Donate'
    start_template = None
    end_template = 'donation.html'
    questions = [
        {
            "component": "ChoiceOtherField",
            "unique": "amount",
            "max": 1,
            "options": get_amounts(),
            "required": True,
            "label": "How much are you able to donate?",
            "description": "Select an amount you are able to donate to our campaign, or click other and enter an amount.",
            "value": []
        },
        {
            "component": "ChoiceField",
            "unique": "contact",
            "max": 1,
            "options": [
                "Yes please", "No"
            ],
            "required": True,
            "label": "May we email you?",
            "description": "Let us know if you're happy for us to send you the occasional email to let you know how"
                           "how we're using your donation, and about campaigning events.",
            "value": []
        }
    ]
    payment = True

    def get_start_page(self, request):
        today = datetime.date.today()
        not_open = today < self.opens_at
        deadline_passed = today > self.deadline

        if deadline_passed or not_open:
            return render(request, 'forms/not_open.html', {
                "form": self,
                "deadline_passed": deadline_passed,
                "not_open": not_open
            })

        return render(request, 'forms/start/' + self.start_template, {
            "form": self
        })

    def get_end_page(self, request):
        token_key = "%s_payment_token" % (self.slug,)
        token = request.session[token_key]

        try:
            payment = Payment.objects.get(token=token)
            if payment.status == 'P':
                body_html = render_to_string('emails/thanks-for-your-donation.html', {
                    "payment": payment,
                    "title": "Thank You"
                })

                email = EmailMessage(
                    subject='Your donation to the Labour Party',
                    body=body_html,
                    from_email='North West Cambs Labour Party <no_reply@northwestcambslabour.org.uk>',
                    to=[
                        '"%s" <%s>' % (payment.name, payment.email)
                    ],
                )

                email.content_subtype = "html"
                email.send(fail_silently=True)

        except:
            pass


        return render(request, 'forms/end/'+self.end_template, {
            "form": self,
            "page": {
                "title": self.title + ' Completed'
            }
        })

    def get_payment_amount(self):
        if "amount[0]" in self.POST:
            return self.POST["amount[0]"]
        else:
            return 0

    def on_submit(self, request):
        token_key = "%s_payment_token" % (self.slug,)

        payment = Payment(
            token=secrets.token_urlsafe(32),
            form=self.slug,
            amount=self.get_payment_amount()
        )

        payment.save()

        request.session[token_key] = payment.token